
var g_kindleMode;

function bijiTextToHtml(lines) {
  if (lines.length == 0) return '';
  var continueMode = false;
  for (var lineIndex = 0, len = lines.length; lineIndex < len; lineIndex++) {
    var line = lines[lineIndex];
    var groups = null;

    // 处理 <code> 开始的块，直到 </code> 结束
    if (line.match('^<code( .*?)?>$')) {
      // 删除 code 标签后面的换行
      if (lineIndex < (len - 1) && ! lines[lineIndex + 1].match('^</code>$')) {
        lines[lineIndex] = '';
        lines[lineIndex + 1] = line + lines[lineIndex + 1];
      }
      for (lineIndex++; lineIndex < len; lineIndex++) {
        line = lines[lineIndex];
        if (line.match('^</code>$')) {
          break;
        }
      }
      continue;
    }

    // 处理 <xmp> 开始的块，直到 </xmp> 结束
    if (line.match('^<xmp( .*?)?>$')) {
      var useHtml = line.match('^<xmp class="html">$');
      line = line.replace(/^<xmp/, useHtml ? '<div' : '<code');
      lines[lineIndex] = line;
      // 删除 xmp 标签后面的换行
      if (!useHtml && lineIndex < (len - 1) && ! lines[lineIndex + 1].match('^</xmp>$')) {
        lines[lineIndex] = '';
        lines[lineIndex + 1] = line + escapeAll(lines[lineIndex + 1]);
        lineIndex++;  // 跳过已经处理的下一行
      }
      for (lineIndex++; lineIndex < len; lineIndex++) {
        line = lines[lineIndex];
        if (line.match('^</xmp>$')) {
          lines[lineIndex] = useHtml ? '</div>' : '</code>';
          break;
        }
        if (!useHtml) {
          lines[lineIndex] = escapeAll(line);
        }
      }
      continue;
    }

    // 处理 <p cn> / <p class="cn-block">，直到 </p class="cn-block"> 或 </p> 结束
    // 需要加上 (</p>)? ，因为上一个 <p> 未结束时，浏览器会自动在 <p> 标签前面加上 </p>。
    // 浏览器会自动将 <p cn> 变为 <p cn="">，将 </p class="cn-block"> 变为 </p>。
    if (line.match('^(</p>)?<p class="cn-block">$')
        || line.match('^(</p>)?<p cn(="")?>$')) {
      for (lineIndex++; lineIndex < len; lineIndex++) {
        line = lines[lineIndex];
        if (line.match('^</p class="cn-block">$') || line.match('^</p( cn)?>$')) {
          break;
        } else if (line.match('^.{1,2}$')) {
          // 一行只有 1-2 个字符时，相当于是另起一段。
          lines[lineIndex - 1] += '</p>';
          lines[lineIndex] = '<p>' + line + '<br>';
        } else {
          addSpaceBeforeContinueLastLine(lines, lineIndex);
          lines[lineIndex - 1] += '<wbr';
          lines[lineIndex] = '>' + lines[lineIndex];
          // console.log(lines[lineIndex - 1]); console.log(lines[lineIndex]); // DEBUG
        }
      }
      continue;
    }

    // 处理 <p> 开始的块，直到 </p> 或空行或其它块元素结束
    if (groups = line.match('^(\\s*(?:</p>)?)(<p(?: .*?)?>.*)$')) {
      lines[lineIndex] = groups[1] + escapeBijiLine(groups[2]);
      for (lineIndex++; lineIndex < len; lineIndex++) {
        line = lines[lineIndex];
        line = escapeBijiLine(line);
        lines[lineIndex] = line;
        if (line.match('</p>$')
            || line.match('<div( .*?)?>|<code( .*?)?>|<ul( .*?)?>|<ol( .*?)?>')) {
          break;
        } else if (line.match('^.{1,2}$')) {
          // 一行只有 1-2 个字符时，相当于是另起一段。
          lines[lineIndex - 1] += '</p>';
          lines[lineIndex] = '<p class="pre-wrap">' + line + '<br>';
        } else if (line.match('^$')) {
          lines[lineIndex - 1] += '</p>';
          lineIndex--;
          break;
        }
      }
      continue;
    }

    // 处理独立的行
    if (!g_kindleMode && (groups = line.match('^( *)(https?://[^ ]+)(.*)$'))) {
      line = '<p class="pre-wrap">' + groups[1]
          + '<a class="break-word" target="_blank" href="' + groups[2] + '">'
          + groups[2] + '</a>' + groups[3] + '</p>';
      lines[lineIndex] = line;
    } else if (line.match('^@(20\\d\\d/)?\\d\\d?/\\d\\d?')) {
      if (g_kindleMode) {
        lines[lineIndex] = '<p class="h3">' + line + '</p>';
      } else {
        lines[lineIndex] = '</div><div data-h2="' + line + '"><h2>' + line + '</h2>';
      }
    } else if (line.match('^\\*{3} ')) {
      if (g_kindleMode) {
        lines[lineIndex] = '<h2>' + line + '</h2>';
      } else {
        lines[lineIndex] = '</div><div data-h3="' + line + '"><h3>' + line + '</h3>';
      }
    } else if (line.match('^----$')) {
      lines[lineIndex] = '<hr>';
    } else if (line.match('^$')) {
      lines[lineIndex] = '<p><br></p>';
    } else if (line.match('^<[^>]*?>$')) {
      // Skip lines like: <div>, <p cn>, </p cn>
      // NOTICE: no space in the beginning of end of line.
    } else {
      var continueThisLine = false;
      line = escapeBijiLine(line);
      if (line.match('<wbr$')) {
        // This may happen in command-line conversion.
        // The next line continues this line.
        continueThisLine = true;
      }
      var moreClass = '';
      if (/^- /.test(line)) {
        moreClass = ' li-1';
      } else if (/^ {2,3}- /.test(line)) {
        moreClass = ' li-2';
      } else if (/^ {4,}- /.test(line)) {
        moreClass = ' li-3';
      }
      if (!continueMode) {
        line = '<p class="pre-wrap' + moreClass + '">' + line;
      }
      if (!continueThisLine) {
        line += '</p>';
      }
      lines[lineIndex] = line;
      continueMode = continueThisLine;
    }
    // console.log(lines[lineIndex]); // DEBUG
  }
  return lines.join('\n') + '\n';

  function escapeAll(line) {
    return line.replace(/&/g, '&amp;')
        .replace(/</g, '&lt;').replace(/>/g, '&gt;');
  }
}

function escapeBijiLine(line) {
  var beginTag = '', endTag = '';
  var groups;
  groups = line.match('^(\\s*>|</p><p(?: .*?)?>|\\s*</?\\w+(?: .*?)?>)(.*)$');
  if (groups) {
    beginTag = groups[1];
    line = groups[2];
  }
  groups = line.match('^(.*)(</?\\w+>?)$');
  if (groups) {
    endTag = groups[2];
    line = groups[1];
  }
  // Don't escape &lt; &gt; &amp; , they are already converted by browser.
  line = line.replace(/&(?!(lt|gt|amp|nbsp);)/g, '&amp;');
  // In browser <wbr> is in a line, and should not be escaped.
  // The b/i/s/u tags should not be escaped either.
  line = line.replace(/<(?!(wbr|\/?[bisu])>)/g, '&lt;').replace(/>/g, '&gt;')
      .replace(/<(wbr|\/?[bisu])&gt;/g, '<$1>');
  // If begin tag starts with space, escape this begin tag,
  // except when line / end tag are empty, and begin tag is <br> .
  if (beginTag.match('^\\s+<')) {
    if (! (beginTag.match('^\\s+<br/?>') && line == '' && endTag == '')) {
      beginTag = beginTag.replace(/</g, '&lt;').replace(/>/g, '&gt;');
    }
  }
  return beginTag + line + endTag;
}

function addSpaceBeforeContinueLastLine(lines, lineIndex) {
  var lastLine = lines[lineIndex - 1];
  var line = lines[lineIndex];
  if (lastLine == '' || line == ''
      || lastLine.match('(</?p( .*?)?>|<br/?>)$')) {
    return;
  }
  var prevChar = lastLine.charAt(lastLine.length - 1);
  if (!isCharCodeCJKOrFullWidthPunctuation(prevChar.charCodeAt(0))) {
    if (! /^\s/.test(line)) {
      lines[lineIndex - 1] += ' ';
    }
  }
}

function isCharCodeCJKOrFullWidthPunctuation(charCode) {
  return (0x3400 <= charCode && charCode <= 0x9FFF)
      // TODO: check the following full-width punctuations ranges
      || (0xFF00 <= charCode && charCode <= 0xFFF0)  // full-width punctuations
      || (0x3000 <= charCode && charCode <= 0x30FF)  // full-width punctuations
      || (0x2010 <= charCode && charCode <= 0x202F)  // full-width punctuations
      || (0xF900 <= charCode && charCode <= 0xFAFF)
      || (0xF900 <= charCode && charCode <= 0xFAFF)
      || (0x20000 <= charCode && charCode <= 0x2FFFF)
      ;
}

(function () {
  var element = document.getElementById('biji1');
  if (element) {
    var lines = element.innerHTML.split('\n');
    element.innerHTML = bijiTextToHtml(lines);
    if (element.className.match(' biji-pre-wrap')) {
      element.className = element.className.replace(' biji-pre-wrap', '');
    }
  }
  /*
  window.resizeTo(505, window.outerHeight);
  // After above line, in Mac's Safari 9.1.3, window.innerWidth = 420.
  // Zoom In once, and the display will fit in the screen width.
  */
})();

