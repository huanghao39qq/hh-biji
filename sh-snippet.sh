## USAGE:
# for i in bash sh zsh dash ksh; do xVERBOSE=1 $i sh-snippet.sh ; done
#
## This does not work in csh or tcsh.

# mini-sh-test begin
fail() {
  echo "Test FAILED:$*" >&2
}
assertEquals() {
  [ "$VERBOSE" ] && echo "$2"
  [ $# = 2 ] || echo 'Test FAILED: WRONG ARGS' >&2
  [ "$1" = "$2" ] || { echo 'Test FAILED:' ; echo "expected:$1" ; echo "  actual:$2" ; }
}
__latest_test=
TESTCASE() { $__latest_test ; __latest_test=$1 ; }
# mini-sh-test end

TESTCASE test_202310
         test_202310() {
  :
}

TESTCASE test_bash_comment_20231110
         test_bash_comment_20231110() {
  assertEquals 'a##' \
        "$(echo a## #comment
          )"
}

TESTCASE test_20231028
         test_20231028() {
  assertEquals '1\t\r\n9' "1\t\r\n9"
  assertEquals '1\t\r\n9' "1\\t\\r\\n9"
  assertEquals '$:' "$:"

  set ABC
  assertEquals 'ABC' "$1"

  # dash does NOT understand $'string'
  # assertEquals '"$`' $'\x22\x24\x60'
  # [ "'" = $'\x27' ] || fail "\$'string'"
}

TESTCASE  # KEEP THIS TO RUN LAST TEST
