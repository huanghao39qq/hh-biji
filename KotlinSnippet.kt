
// Usage:
// kotlinc KotlinSnippet.kt && kotlin -cp . KotlinSnippetKt
// ./gradlew run --args="-v"
//
// javap -cp . KotlinSnippet
// javap -cp . KotlinSnippet.Person

import java.io.Closeable
import java.util.NoSuchElementException
import kotlin.reflect.KClass
import kotlin.reflect.KProperty
import kotlin.reflect.full.memberFunctions
import kotlin.reflect.full.primaryConstructor
import kotlin.system.exitProcess

class KotlinSnippet {

  fun test_202405() {
  }

  // https://kotlinlang.org/docs/functions.html#default-arguments
  fun test_function_default_argument_20240507() {
    fun read(
        b: String,
        off: Int = 0,
        len: Int = b.length
    ): String = "$b $off $len"

    assertEquals("abc 0 3", read("abc"))
    assertEquals("abc 0 5", read("abc", len = 5))

    /*
    fun read2(
        off: Int = 0,
        len: Int = b.length, // error: parameter 'b' is uninitialized here
        b: String
    ): String = "$b $off $len"
    */
  }

  fun test_list_add_20240410() {
    var numbers = listOf(1, 2)
    assertEquals("[1, 2, 3, 4]", (numbers + listOf(3, 4)).toString())
  }

  // https://kotlinlang.org/docs/java-to-kotlin-idioms-strings.html
  fun test_buildString_20240403() {
    var str: String
    str = buildString {
      append("a")
      append("b")
      // it.append("b")   // error: unresolved reference: it
      // appendLine()     // error: unresolved reference: appendLine
      for (i in 5 downTo 1) {
        append(i)
      }
    }
    assertEquals("ab54321", str)

    val numbers = listOf(1, 2, 3, 4, 5, 6)
    str = numbers
        .filter { it % 2 != 0 }
        .joinToString(separator = ";") {"${-it}"}
    assertEquals("-1;-3;-5", str)
  }

  class MyDelegate {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
      return "$thisRef, thank you for delegating '${property.name}' to me!"
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
      log.append("$value has been assigned to '${property.name}' in $thisRef.")
    }

    companion object {
      val log = StringBuilder()
    }
  }

  // https://kotlinlang.org/docs/delegated-properties.html
  fun test_delegated_properties_20240322() {
    val log = StringBuilder()
    val obj = object {
      var p: String by MyDelegate()
      override fun toString() = "foo" // p.getValue()
      val lazyValue: String by lazy {
        log.append("computed!")
        "Hello"
      }
    }

    assertEquals("foo, thank you for delegating 'p' to me!", obj.p)
    MyDelegate.log.clear()
    obj.p = "def"
    assertEquals("foo, thank you for delegating 'p' to me!", obj.p)
    assertEquals("def has been assigned to 'p' in foo.", MyDelegate.log.toString())

    assertEquals("", log.toString())
    assertEquals("Hello", obj.lazyValue)
    assertEquals("computed!", log.toString())
    assertEquals("Hello", obj.lazyValue)
    assertEquals("computed!", log.toString())
  }

  // https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/flat-map.html
  fun test_flatMap_20240321() {
    assertListEquals(listOf('a', 'b', 'c'), "abc".toList())

    val list = listOf("abc", "de")
    assertListEquals(listOf('a', 'b', 'c', 'd', 'e'), list.flatMap { it.toList() })

    // TODO
    // val toUpper: (String) -> List<String> = { it: String -> listOf(it.toUpperCase()) }
    // assertListEquals(listOf(""), list.flatMap { toUpper })
  }

  // https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-abstract-list/equals.html
  fun test_assertListEquals_20240321() {
    assertListEquals(listOf(2, 3), mutableListOf(2, 3))
    assertListEquals(listOf("one", "two"), mutableListOf("one", "two"))

    if (listOf(2, 3) != mutableListOf(2, 3)) fail()
    if (! listOf(2, 3).equals(mutableListOf(2, 3))) fail()
  }

  // https://kotlinlang.org/docs/extensions.html
  fun test_extensions_20240319() {
    // error: unresolved reference: beginsWith
    // assertTrue("abc".beginsWith("a"))

    fun String.beginsWith(s: String): Boolean {
      return this.startsWith(s)
    }
    assertTrue("abc".beginsWith("a"))

    // 2024-04-09
    fun String.beginsWith2(s: String): Boolean {
      return startsWith(s)
    }
    assertTrue("abc".beginsWith2("a"))
  }

  // https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.text/trim-margin.html
  // https://kotlinlang.org/docs/java-to-kotlin-idioms-strings.html
  fun test_trimMargin_trimIndent_20240319() {
    var str =
      """
      |ABC
      |123
      |456
      """
    assertEquals("\n      |ABC\n      |123\n      |456\n      ", str)
    assertEquals("ABC\n123\n456", str.trimMargin())

    // 20240403
    str = """
      ABC
        123
      """
    assertEquals("\n      ABC\n        123\n      ", str)
    assertEquals("ABC\n  123", str.trimIndent())
  }

  // https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.text/-regex/
  fun test_Regex_20240318() {
    var pattern = Regex("a.")

    // matches: Indicates whether the regular expression matches the entire input.
    assertTrue("ab".matches(pattern))
    assertTrue(! "abc".matches(pattern))
    assertTrue(! "Abc".matches(pattern))
    assertTrue(pattern.matches("ab"))
    assertTrue(! pattern.matches("abc"))

    var matchResult = pattern.matchEntire("abc")
    assertTrue(null == matchResult)
    assertTrue(null == matchResult?.value)
    matchResult = pattern.matchEntire("ab")
    assertEquals("ab", matchResult?.value)

    assertTrue(pattern.find("abc") != null)

    // containsMatchIn: Indicates whether the regular expression can find
    //                  at least one match in the specified input.
    assertTrue(pattern.containsMatchIn("abc"))
  }

  fun test_emptyList_20240318() {
    assertEquals(0, emptyList<Int>().size)
    assertEquals(0, listOf<String>().size)
  }

  // https://kotlinlang.org/docs/functions.html
  fun test_default_and_named_arguments_20240318() {
    fun read(b: ByteArray, off: Int = 0, len: Int = b.size): String {
      return "off=" + off + " len=" + len
    }
    var byteArray = byteArrayOf(0x30, 0x31, 0x32)
    assertEquals("off=0 len=3", read(byteArray))
    assertEquals("off=1 len=3", read(byteArray, 1))
    assertEquals("off=0 len=1", read(byteArray, len = 1))

    fun foo(vararg strings: String): String {
      var result = ""
      for (s in strings) {
        result += s + ":"
      }
      return result
    }
    assertEquals("a:b:c:", foo(strings = *arrayOf("a", "b", "c")))
  }

  // https://kotlinlang.org/docs/null-safety.html
  fun test_apply_also_run_let_20240317() {
    // TODO:
    // val numbers = listOf(1, 2, 3)

    fun String.dummy(): String {
      return "" + this.length + " " + length + " " + this
    }
    assertEquals("3 3 abc", "abc".dummy())
  }

  // https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-pair/
  fun test_Pair_20240315() {
    var pair: Pair<Int, String>
    pair = Pair(1, "one")
    assertEquals("(1, one)", pair.toString())
    assertEquals(1, pair.first)
    assertEquals("one", pair.second)
    val (a, b) = pair
    assertEquals(1, a)
    assertEquals("one", b)
  }

  fun number_op(n: Int, op: (Int) -> Int): Int {
    return op(n)
  }

  // https://kotlinlang.org/docs/lambdas.html
  fun test_higher_order_functions_20240315() {
    assertEquals(5, number_op(2, { n -> n + 3 }))
    var op: (num: Int) -> Int
    op = { n -> n * 2 }
    assertEquals(6, number_op(3, op))

    var a: () -> String
    a = { "dummy" }
    assertTrue(null == a::class.simpleName)
    assertTrue(null == a::class.qualifiedName)
    // assertTrue(a::class.isFun)

    a = fun(): String { return "A" }
    assertEquals("A", a())
    assertEquals("A", a.invoke())

    val repeatFun: String.(Int) -> String = { times -> this.repeat(times) }
    assertEquals("abcabc", "abc".repeatFun(2))

    val repeatFunB: String.(Int) -> String = { times -> repeat(times) }
    assertEquals("abcabc", "abc".repeatFunB(2))

    var add = { x: Int, y: Int -> x + y }
    assertEquals(5, add(2, 3))

    // typealias FuncA = (Int?) -> Unit
  }

  class MyFile(val name: String, val log: StringBuilder) : Closeable {
    fun print() {
      log.append("printing\n")
    }
    override fun close(): Unit {
      log.append(name + " closed.\n")
    }
  }

  // https://stackoverflow.com/questions/26969800/try-with-resources-in-kotlin
  fun test_use_Closeable_20240314() {
    val log = StringBuilder()
    MyFile("A", log).use { it.print() }
    assertEquals("printing\nA closed.\n", log.toString())
  }

  // https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/require.html
  // https://kotlinlang.org/docs/jvm-api-guidelines-predictability.html#validate-inputs-with-the-require-function
  // https://stackoverflow.com/questions/53139249/what-is-the-difference-between-require-and-assert
  fun test_require_check_20240314() {
    var n = 5
    require(n > 3) { "Number must be greater than 3" }
    try {
      require(n > 5)
      fail()
    } catch (e: java.lang.IllegalArgumentException) {
    }

    try {
      check(n > 5)
      fail()
    } catch (e: java.lang.IllegalStateException) {
    }
  }

  // https://kotlinlang.org/docs/collection-transformations.html
  fun test_transformations_20240314() {
    val numbers = listOf(1, 2, 3)
    assertEquals("[3, 9]",
        numbers.mapNotNull { if ( it == 2) null else it * 3 }.toString())
    assertEquals("[0, 2, 6]",
        numbers.mapIndexed { idx, value -> value * idx }.toString())

    val list = listOf("one", "two", "three")
    assertEquals("[(1, one), (2, two), (3, three)]",
        (numbers zip list).toString())

    val numberPairs = listOf("one" to 1, "two" to 2, "three" to 3)
    assertEquals(3, numberPairs.size)
    val (a, _) = numberPairs.unzip()
    assertEquals("[one, two, three]", a.toString())

    assertEquals("3", list.associateWith { it.length }["two"].toString())

    val numberSets = listOf(setOf(1, 2, 3), setOf(4, 5), setOf(1, 2))
    assertEquals("[1, 2, 3, 4, 5, 1, 2]", numberSets.flatten().toString())

    assertEquals("1, 2, 3", numbers.joinToString())
    assertEquals("1:2:3", numbers.joinToString(":"))
    assertEquals("start: 1 | 2 | 3: end",
        numbers.joinToString(separator = " | ", prefix = "start: ", postfix = ": end"))
    assertEquals("1, 2, (...)",
        numbers.joinToString(limit = 2, truncated = "(...)"))
    assertEquals("1, 2, 3",
        numbers.joinToString(limit = 3, truncated = "(...)"))
  }

  // https://kotlinlang.org/docs/ranges.html
  // https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.ranges/-int-range/
  // https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.ranges/-long-range/
  // https://stackoverflow.com/questions/57682305/kotlin-what-happens-when-a-range-is-created-using-or-how-it-performs-compar
  fun test_range_and_progression() {
    assertTrue(5 in 1..5)
    assertTrue(! (5 in 1 until 5))
    assertTrue(5 !in 1 until 5)    // NO space between ! and in
    // assertTrue(5 !in 1..<5)
    assertTrue(5 in 5 downTo 1)

    assertTrue(! (5 in 0..10 step 2))

    var range: IntRange
    range = 1..5
    assertEquals(5, range.endInclusive)
    assertTrue(range.contains(5))

    assertEquals("class kotlin.ranges.IntProgression",
        (5 downTo 1)::class.toString())

    val list = mutableListOf<Int>()
    for (i in 5 downTo 1 step 2) {
      list.add(i)
    }
    assertEquals("[5, 3, 1]", list.toString())
  }

  // https://kotlinlang.org/docs/control-flow.html
  fun test_when_switch_20240314() {
    var str: String
    var n = 5
    when (n) {
      1 -> str = "a"
      in 2..4 -> str = "b"
      3, 4, 5 -> str = "c"
      in 5..6 -> str = "d"
      else -> str = "n"
    }
    assertEquals("c", str)

    str = when {
      n > 5 -> "A"
      else -> "B"
    }
    assertEquals("B", str)

    fun return_when(n: Int): String {
      return when {
        n in 2..4 -> "aa"
        n > 5 -> "bb"
        else -> "cc"
      }
    }
    assertEquals("cc", return_when(5))
    assertEquals("aa", return_when(4))
  }

  // class T public @Inject constructor(name: String)

  data class Addr(
    var street: String,
    val city: String
  )

  class AddrPair constructor(val addr1: Addr, var addr2: Addr) {
  }

  class Person constructor(name: String) {
    val firstName = name
    val fullName: String
    var addr: Addr? = null
    init {
      fullName = name
    }
    constructor() : this("noname") {
    }
    override fun toString(): String {
      return fullName
    }
    fun hello(): String = "Hi! I am " + fullName + "."
  }

  // https://kotlinlang.org/docs/classes.html
  fun test_class_basics_20240314() {
    var p = Person("Foo")
    assertEquals("Foo", p.fullName)
    assertEquals("Hi! I am Foo.", p.hello())
    p = Person()
    assertEquals("noname", p.toString())
  }

  // https://kotlinlang.org/docs/arrays.html
  fun test_array_20240314() {
    var a = arrayOf(1, 2, 3)
    assertEquals(3, a.size)
    assertTrue(a contentEquals arrayOf(1, 2, 3))
    assertTrue(a.contentEquals(arrayOf(1, 2, 3)))
  }

  // https://kotlinlang.org/docs/collections-overview.html#map
  fun test_map_20240314() {
    var map = mapOf("key1" to 1, "key2" to 2, "key3" to 3, "key4" to 1)
    assertEquals(2, map["key2"])

    map = mutableMapOf("one" to 1, "two" to 2)
    map.put("three", 3)
    assertEquals(3, map["three"])

    val (a, b) = map.entries.partition { (key, _) -> key == "two" }
    assertEquals("[two=2]", a.toString())
    assertEquals("[one=1, three=3]", b.toString())

    // 2024-05-07
    // https://stackoverflow.com/questions/47204146/how-to-iterate-over-hashmap-in-kotlin
    val log = StringBuilder()
    for ((k, v) in map) {
      log.append("$k $v ")
    }
    assertEquals("one 1 two 2 three 3 ", log.toString())

    log.setLength(0)
    map.forEach { (k, v) -> log.append("$k,$v,") }
    assertEquals("one,1,two,2,three,3,", log.toString())

    log.setLength(0)
    map.forEach { entry -> log.append("${entry.key}-${entry.value}-") }
    assertEquals("one-1-two-2-three-3-", log.toString())

    log.setLength(0)
    for (entry in map.iterator()) {
      log.append(entry.key).append(entry.value)
    }
    assertEquals("one1two2three3", log.toString())

    // https://kotlinlang.org/docs/map-operations.html
    map = mutableMapOf("one" to 1, "two" to 2)
    assertEquals("{one=1, two=2, four=4}", (map + Pair("four", 4)).toString())
    assertEquals("{two=2}", (map - "one").toString())
    assertEquals("{one=1}", (map - listOf("two", "four")).toString())
    map.putAll((setOf("four" to 4, "five" to 5)))
    assertEquals("{one=1, two=2, four=4, five=5}", map.toString())
  }

  // https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/partition.html
  fun test_partition_20240314() {
    val array = intArrayOf(1, 2, 3, 4, 5)
    val (even, odd) = array.partition { it % 2 == 0 }
    assertEquals("[2, 4]", even.toString())
    assertEquals("[1, 3, 5]", odd.toString())
  }

  fun test_list_map_any_all_20240314() {
    var list = listOf("a3", "a2", "b1")
    assertEquals(3, list.size)
    assertEquals("[A3, A2, B1]", list.map { it.toUpperCase() }.toString())
    assertEquals("[a3, a2]", list.filter { it.startsWith("a") }.toString())
    assertTrue(list.any { it == "b1" })
    assertTrue(list.all { it.length == 2 })
    assertTrue(! list.contains("c"))
    assertTrue(list.containsAll(listOf("a2", "b1")))

    assertEquals("a2", list.filter { it.startsWith("a") }.last())
    assertEquals("N", list.filter { it.startsWith("c") }.lastOrNull() ?: "N")
    try {
      list.filter { it.startsWith("c") }.last()
      fail()
    } catch (e: NoSuchElementException) {
    }

    assertEquals("[A2, A3]",
        list
        .filter { it.startsWith("a") }
        .map { it.toUpperCase() }
        .sortedBy { it }
        .toString())
  }

  // https://kotlinlang.org/docs/null-safety.html
  fun test_null_safety_20240312() {
    var a: String = "abc" // Regular initialization means non-nullable by default
    // a = null // compilation error
    assertEquals("abc", a)

    var b: String? = "abc" // can be set to null
    var len = if (b != null) b.length else -1
    assertEquals(3, len)
    if (a == "abc") b = null // ok
    assertEquals("null", "" + b)
    len = if (b != null) b.length else -1
    assertEquals(-1, len)

    // https://kotlinlang.org/docs/null-safety.html#safe-calls
    assertEquals("null", "" + b?.length)
    var n = 1
    b?.let { n = 2 }
    assertEquals(1, n)
    a.let { n = it.length }
    assertEquals(3, n)

    // https://kotlinlang.org/docs/null-safety.html#elvis-operator
    len = b?.length ?: -2
    assertEquals(-2, len)

    // // Since throw and return are expressions in Kotlin, they can also be used
    // // on the right-hand side of the Elvis operator.
    // val parent = node.getParent() ?: return null
    // val name = node.getName() ?: throw IllegalArgumentException()
  }

  // https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/filter-not.html
  fun test_list_filter_20240311() {
    val numbers: List<Int> = listOf(1, 2, 3, 4, 5, 6, 7)
    val evenNumbers = numbers.filter { it % 2 == 0 }
    val notMultiplesOf3 = numbers.filterNot { number -> number % 3 == 0 }
    assertEquals("[2, 4, 6]", evenNumbers.toString())
    assertEquals("[1, 2, 4, 5, 7]", notMultiplesOf3.toString())

    val a: List<Int> = listOf(1, 3, 2)
    assertEquals("[3, 2, 1]", a.sortedByDescending{ it }.toString())
    assertEquals(1, a.firstOrNull{ it == it })
  }

  // Put the following tests in the end, since they may break syntax highlighting.

  // https://kotlinlang.org/docs/strings.html#multiline-strings
  // https://kotlinlang.org/docs/strings.html#string-templates
  fun test_multiline_string_20240411() {
    var str = "abc"
    assertEquals("\\abc = abc", """\$str = $str""")
    assertEquals("\$str = abc", """${'$'}str = $str""")

    // NOTICE: Multiline string contains no escaping and can contain newlines.
    assertEquals("\\d\\n", """\d\n""")
    assertEquals("a\n        \$\$ abc ABC", """a
        $$ $str ${str.toUpperCase()}""")
  }

}

// mini-test begin

var _verbose = false
var _failed = false

fun fail() { fail("Test FAILED!") }

fun fail(message: String) {
  _failed = true
  System.err.println(message)
}

fun assertTrue(b: Boolean) { if (!b) fail ("assertTrue failed") }
fun assertFalse(b: Boolean) { if (b) fail ("assertFalse failed") }

fun <T> assertListEquals(expected: List<T>, actual: List<T>) {
  /*
  // https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-abstract-list/equals.html
  // List has a working "structural equality (==)" method.
  var result = (expected.size == actual.size)
  if (result) {
    for (p in expected.zip(actual)) {
      if (p.first == p.second) continue
      result = false
      break
    }
  }
  */
  if (expected == actual) {
    if (_verbose) println(actual)
    return
  }
  fail("expected:" + expected + "\n  actual:" + actual)
}

fun <T> assertEquals(expected: T, actual: T?) {
  if (expected == actual) {
    if (_verbose) println(actual)
    return
  }
  fail("expected:" + expected + "\n  actual:" + actual)
}

fun main(args: Array<String>, kClass: KClass<out Any>) {
  if (args.size > 0 && "-v" == args[0]) _verbose = true
  val constructor1 = kClass.primaryConstructor
  val functions = kClass.memberFunctions
  functions.forEach { f ->
    if (f.name.startsWith("test")) {
      if (_verbose) println("=== ${f.name}")
      val instance = constructor1!!.call()
      f.call(instance)
    }
  }
  if (_failed) exitProcess(1)
}

// mini-test end

fun main(args: Array<String>) {
  main(args, KotlinSnippet::class)
}
