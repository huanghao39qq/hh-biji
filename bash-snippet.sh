
# mini-test begin
function fail() {
  echo "Test FAILED:(${BASH_LINENO[0]}):$*" >&2
}
function assertEquals() {
  local newline=$'\n'
  local n=${BASH_LINENO[0]}
  [ "$VERBOSE" ] && echo "($n):${2//$newline/\\n}"
  [ $# = 2 ] || echo "Test FAILED:($n): WRONG ARGS" >&2
  [ "$1" = "$2" ] || echo "Test FAILED:($n):"\
$'\n'"expected:${1//$newline/\\n}"\
$'\n'"  actual:${2//$newline/\\n}" >&2
}
__latest_test=
function TESTCASE() { $__latest_test ; __latest_test=$1 ; }
# mini-test end

TESTCASE test_202311
function test_202311() {
  :
}

TESTCASE test_20250113_substring
function test_20250113_substring() {
  a='ABCDEFG'
  assertEquals 'ABC' "${a:0:3}"
  assertEquals 'BCD' "${a:1:3}"
  assertEquals '' "${a:1:0}"
  assertEquals 'ABCDEFG' "${a:-1}"
  assertEquals 'G' "${a: -1}"
  assertEquals 'DEF' "${a: -4:3}"
  assertEquals 7 "${#a}"
  n=2
  assertEquals 'BCDE' "${a:n-1:n+2}"
}

TESTCASE test_echo_printf_20231114
function test_echo_printf_20231114() {
  assertEquals '310a'     "$(echo  $'1\0 2' | xxd -p )"
  assertEquals '31002032' "$(printf '1\0 2' | xxd -p )"
}

TESTCASE test_bash_special_var_20231114
function test_bash_special_var_20231114() {
  # LINENO is special, it cannot be exported!
  #
  # man bash | less -p 'Shell Variables$'
  # https://www.gnu.org/software/bash/manual/html_node/Bash-Variables.html
  assertEquals '0' \
    "$(LINENO=5 bash -c 'echo $LINENO' )"
}

TESTCASE test_lazy_evaluation_20231112
function test_lazy_evaluation_20231112() {
  assertEquals 'A' \
    "$( true && echo -n A || echo -n B )"
  assertEquals 'AB' \
    "$( true && echo -n A || true && echo -n B )"
  assertEquals 'A' \
    "$( true && echo -n A || { true && echo -n B ; } )"
}

TESTCASE test_bash_comment_20231110
function test_bash_comment_20231110() {
  assertEquals 'a##' \
        "$(echo a## #comment
          )"
}

TESTCASE test_uniq_20231107
function test_uniq_20231107() {
  assertEquals $'   2 1' \
      "$(echo $'1\n1' | uniq -c )"

  assertEquals $'   2 1\n   1 ' \
      "$(echo $'1\n1\n' | uniq -c )"

  assertEquals $'   2 1\n   2 ' \
      "$(echo $'1\n1\n\n' | uniq -c )"
}

TESTCASE test_xxd_20231107
function test_xxd_20231107() {
  assertEquals '0f01' \
      "$(echo "0    f" 01 a | xxd -r -p | xxd -p )"

  # xxd -r stops at invalid chars
  assertEquals '0f' \
      "$(echo "0    f" "#junk" 01 a | xxd -r -p | xxd -p )"
}

TESTCASE test_read_and_group_redirect_20231103
function test_read_and_group_redirect_20231103() {
  while IFS= read -r line ; do
    assertEquals '  a    \"b  ' "$line"
  done <<<       '  a    \"b  '

  while read -r line ; do
    assertEquals 'a    \"b' "$line"
  done <<<       '  a    \"b  '

  while read line ; do
    assertEquals 'a    "b' "$line"
  done <<<       '  a    \"b  '
}

TESTCASE test_tail_20231030
function test_tail_20231030() {
  local a
  a="$(echo ABC | tail -c1 )"
    assertEquals "" "$a"
  a="$(echo ABC | tail -c2 )"
    assertEquals "C" "$a"
    assertEquals 1 "${#a}"
  a="$(echo -n ABC | tail -c2 )"
    assertEquals "BC" "$a"
    assertEquals 2 "${#a}"
  a="$(echo -n | tail -c1 )"
    assertEquals "" "$a"
}

TESTCASE test_sed_20231020
function test_sed_20231020() {
  assertEquals $'1 A\n#2 b\n#3 c' \
      "$( echo $'1 a\n2 b\n3 c' | sed -e 's/a/A/ ; t' -e 's/^/#/' )"

  # 多行替换
  assertEquals '1-2-3-4-5' \
      "$( seq 5 | sed -n 'H;$x;$s/\n//;$s/\n/-/g;$p' )"
  assertEquals $'1\n-\n4\n5' \
      "$( seq 5 | sed -E -n 'H;$x;$s/\n//;$s/2\n3/-/g;$p' )"
  assertEquals $'1\n4\n5'    "$( seq 5 | seq 5 | sed '2N;3d' )"
  assertEquals \
      $'1\n2-\n4\n5' \
      "$( seq 5 | sed '2N;3s/\n./-/' )"
  assertEquals \
      $'1\n2-\n4\n5' \
      "$( seq 5 | sed -e '2{N;s/\n./-/' -e } )"

  # 检查 "$( )" 里面的变量替换，是与单独输入一致的，不会嵌套
  local s=ABC
  assertEquals '$s ABC' "$(echo '$s' $s)"
  assertEquals "(echo 'ABC' ABC)" "(echo '$s' $s)"
  assertEquals '"' "$(echo '"')"
  assertEquals '""' "$(echo '""')"

  # $() 可以嵌套
  assertEquals a "$(echo $(echo $(echo a) ) )"
}

TESTCASE test_try_better_code_style_20231020
function test_try_better_code_style_20231020() {
  assertEquals 1 \
  ` echo 1 | cat `

  # WRONG ARGS
  # assertEquals 1 ` echo "A $((1+2*3))" | sed 's/$/"/' `
}

TESTCASE test_trap_20231020
function test_trap_20231020() {
  trap "" 2 3
  trap "my_exit" 2 3
  trap 2 3
  function my_exit() {
    echo OK
  }
}

TESTCASE test_nested_function_20231020
function test_nested_function_20231020() {
  function f() { echo A ; }
  assertEquals A "$(f)"
  function f() { echo B ; }
  assertEquals B "$(f)"
}

TESTCASE test_case_20231020
function test_case_20231020() {
  local a=YEAH str=""
  case $a in
    Y*|y*) str=yes ;;
    N|n) str=no ;;
    *) str="" ;;
  esac
  assertEquals yes "$str"
}

TESTCASE test_fgrep_20231019
function test_fgrep_20231019() {
  assertEquals '1??' "$( echo $'1??\n2**\n3++' | fgrep '??'     )"
  assertEquals ''    "$( echo $'1??\n2**\n3++' | fgrep -x '??'  )"
  assertEquals '1??' "$( echo $'1??\n2**\n3++' | fgrep -x '1??' )"

  assertEquals $'1??\n3++' "$( echo $'1??\n2**\n3++' | fgrep $'?\n+' )"
}

TESTCASE test_array_20231019
function test_array_20231019() {
  local arr n m
  arr=(A 11 22 33 44)
  n=3
  assertEquals 33 "${arr[n]}"
  n=2
  m=n
  assertEquals 22 "${arr[m]}"
  unset n
  assertEquals A "${arr[m]}"
  n=2
  assertEquals 11 "${arr[m-1]}"
  assertEquals 44 "${arr[m+2]}"
  unset m
  assertEquals 22 "${arr[m+2]}"

  arr=(10 20)
  arr+=2
  assertEquals "102 20" "${arr[*]}"
  arr[1]+=2
  assertEquals "102 202" "${arr[*]}"
  arr+=(30 40)
  assertEquals "102 202 30 40" "${arr[*]}"
  arr=(1 2)
  arr+=($arr)
  assertEquals "1 2 1" "${arr[*]}"
  arr+=(${arr[@]})
  assertEquals "1 2 1 1 2 1" "${arr[*]}"
}

TESTCASE test_read_20231019
function test_read_20231019() {
  local arr
  read -a arr <<< "A 11 22"

  assertEquals A "${arr[0]}"
  assertEquals "A 11 22" "${arr[*]}"
  assertEquals 3 ${#arr[@]}
  assertEquals 1 ${#arr}
}

TESTCASE test_sed_20231018
function test_sed_20231018() {
  # man re_format  # MacOS
  # For enhanced basic REs, ‘+’, ‘?’ and ‘|’ remain regular characters, but
  # ‘\+’, ‘\?’ and ‘\|’ have the same special meaning as the unescaped
  # characters do for extended REs

  # We should NEVER use basic RE! Always use extended RE when in doubt.

  assertEquals 1 "$(echo '??1' | sed 's/??//' )"
  assertEquals 1 "$(echo '??1' | sed -E 's/\?\?//' )"

  a=123
  assertEquals '1\n3' "${a/2/\\n}"
}

TESTCASE test_declare_20231018
function test_declare_20231018() {
  local a arr str
  str="1    2"
  a="($str)"
  [[ "$a" == "(1    2)" ]] || fail "$a"
  assertEquals "(1    2)" "$a"
  arr=($str)
  [[ "$arr" == "1" ]] || fail "$arr"
  unset arr
  arr=("$str")
  [[ "$arr" == "1    2" ]] || fail "$arr"

  str='1 "2" $(echo 333)'
  unset a
  declare "a=($str)"
  [[ "$a" == "($str)" ]] || fail "$a"

  unset arr
  declare arr=($str)
  [[ "${arr[1]}" == '"2"' ]] || fail "$(declare -p arr)"
  [[ "${arr[2]}" == '$(echo' ]] || fail "$(declare -p arr)"

  # NOTICE: the command in $str is executed like eval!
  unset arr
  declare -a "arr=($str)"
  [[ "${arr[1]}" == '2' ]] || fail "$(declare -p arr)"
  [[ "${arr[2]}" == "333" ]] || fail "$(declare -p arr)"
  unset arr
  eval "arr=($str)"
  [[ "${arr[2]}" == "333" ]] || fail "$(declare -p arr)"
}

TESTCASE test_command_substitution_20231017
function test_command_substitution_20231017() {
  # $( ) will eat all the trailing newline
  local a str
  a=$( cat <<< 123 )
  assertEquals 123 "$a"

  str=$'1  a\n2  b\n\n'
  a=$( echo "$str" )
  assertEquals $'1  a\n2  b' "$a"
}

TESTCASE  # KEEP THIS TO RUN LAST TEST
