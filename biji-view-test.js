
TESTCASE(testEscapeBijiLine);
function testEscapeBijiLine() {
  assertEquals('special &lt; &gt; &amp; chars', escapeBijiLine(
               'special < > & chars'));
  assertEquals('escaped &lt; &gt; &amp; &nbsp; chars', escapeBijiLine(
               'escaped &lt; &gt; &amp; &nbsp; chars'));
  assertEquals('if you want &amp;amp;', escapeBijiLine(
               'if you want &amp;amp;'));

  assertEquals('<p>&lt; &gt;</p>', escapeBijiLine('<p>< ></p>'));
  assertEquals('</p><p>A</p>', escapeBijiLine('</p><p>A</p>'));
  assertEquals(' &lt;/p&gt;&lt;p&gt;A</p>', escapeBijiLine(' </p><p>A</p>'));
  assertEquals(' ><p>', escapeBijiLine(' ><p>'));
  assertEquals('<p><p>', escapeBijiLine('<p><p>'));
  assertEquals('<p>&lt;p&gt;<p>', escapeBijiLine('<p><p><p>'));
  assertEquals('<div class="a">', escapeBijiLine('<div class="a">'));

  assertEquals('a<div_123', escapeBijiLine('a<div_123'));
  assertEquals('a</div', escapeBijiLine('a</div'));
  assertEquals('a&lt;div class="c"', escapeBijiLine('a<div class="c"'));
  assertEquals('a&lt;div-', escapeBijiLine('a<div-'));

  // TODO: img 标签处理 (2019-9-11)
  /*
  assertEquals('<img src="..." >', escapeBijiLine('<img src="..." >'));
  assertEquals('<img src="..." />', escapeBijiLine('<img src="..." />'));
  assertEquals('<img src="', escapeBijiLine('<img src="'));
  assertEquals('...">', escapeBijiLine('...">'));
  */

  assertEquals(' &lt;p&gt;', escapeBijiLine(' <p>'));
  assertEquals(' &lt;/p&gt;', escapeBijiLine(' </p>'));
  assertEquals(' 1<p>', escapeBijiLine(' 1<p>'));
  assertEquals(' &lt;div class="a"&gt;', escapeBijiLine(' <div class="a">'));
  assertEquals(' &lt;div class="a"&gt;<br>', escapeBijiLine(' <div class="a"><br>'));
  assertEquals(' &lt;div class="a"&gt;&lt;br&gt; ', escapeBijiLine(' <div class="a"><br> '));

  // <br> is special in the end
  assertEquals(' <br>', escapeBijiLine(' <br>'));
  assertEquals(' &lt;br&gt; ', escapeBijiLine(' <br> '));
  assertEquals(' &lt;br&gt;<br>', escapeBijiLine(' <br><br>'));
  assertEquals(' &lt;br&gt;a<br>', escapeBijiLine(' <br>a<br>'));
}

TESTCASE(testEscapeBijiLine_special);
function testEscapeBijiLine_special() {
  assertEquals('&lt; &gt; &amp; &amp;lt', escapeBijiLine('&lt; &gt; &amp; &lt'));

  assertEquals('a<i> </i> <b> </b> <u> </u> <s> </s>b', escapeBijiLine(
      'a<i> </i> <b> </b> <u> </u> <s> </s>b'));

  assertEquals('a<wbr>b', escapeBijiLine('a<wbr>b'));
  assertEquals('a&lt;wbr/&gt;b', escapeBijiLine('a<wbr/>b'));

  assertEquals('a<i> </i', escapeBijiLine('a<i> </i'));
  assertEquals('<i> </i', escapeBijiLine('<i> </i'));
  assertEquals(' &lt;i&gt; </i', escapeBijiLine(' <i> </i'));
}

TESTCASE(testBijiTextToHtml);
function testBijiTextToHtml() {
  assertEquals(
    ['', '<code class="break-word">  var i;', '</code>', ''].join('\n'), bijiTextToHtml(
    ['<code class="break-word">', '  var i;', '</code>']));
  assertEquals(
    ['<p>', 'English', 'paragraph:</p>', '<p><br></p>', ''].join('\n'), bijiTextToHtml(
    ['<p>', 'English', 'paragraph:', '']));
  assertEquals(
    ['<p>', 'English', 'paragraph:</p>', '<p class="pre-wrap">First, ...</p>', ''].join('\n'), bijiTextToHtml(
    ['<p>', 'English', 'paragraph:</p>', 'First, ...']));
  assertEquals(
    ['<p cn><wbr', '>　　中文段落行与行之间<wbr', '>没有空格', '</p cn>', ''].join('\n'), bijiTextToHtml(
    ['<p cn>', '　　中文段落行与行之间', '没有空格', '</p cn>']));
  assertEquals(
    ['<p cn><wbr', '>以英文<wbr', '>English <wbr', '>结尾自动添加空格', '</p cn>', ''].join('\n'), bijiTextToHtml(
    ['<p cn>', '以英文', 'English', '结尾自动添加空格', '</p cn>']));
  assertEquals(
    ['<p cn><wbr', '>第1行</p>', '<p>||<br><wbr', '>第3行', '</p cn>', ''].join('\n'), bijiTextToHtml(
    ['<p cn>', '第1行', '||', '第3行', '</p cn>']));

  assertEquals(
    ['', '<code>&lt;p&gt;abc&lt;/p&gt;', '- &lt;xmp&gt;...&lt; /xmp&gt;', '</code>', ''].join('\n'), bijiTextToHtml(
    ['<xmp>', '<p>abc</p>', '- <xmp>...< /xmp>', '</xmp>']));

  assertEquals(
    ['<code>', '</code>', ''].join('\n'), bijiTextToHtml(
    ['<code>', '</code>']));
  assertEquals(
    ['<code class="break-word">', '</code>', ''].join('\n'), bijiTextToHtml(
    ['<xmp class="break-word">', '</xmp>']));

  assertEquals(
    ['<div class="html">', '<ol><li>1</li></ol>', '</div>', ''].join('\n'), bijiTextToHtml(
    ['<xmp class="html">', '<ol><li>1</li></ol>', '</xmp>']));
}

//----
if ("function" == typeof allTestsLocalRunner) allTestsLocalRunner();
