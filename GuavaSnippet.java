
/**
 * Build and run:
 * p=~/Downloads
 * javac -cp $p/guava-31.1-jre.jar GuavaSnippet.java
 * java -cp .:$p/guava-31.1-jre.jar:$p/failureaccess-1.0.1.jar GuavaSnippet
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Iterators;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutionException;

public class GuavaSnippet {

  public static void test_202403() {
  }

  public static class EventListener {
    StringBuffer log = new StringBuffer();

    @Subscribe
    public void stringEvent(String event) {
      log.append(event + "\n");
    }

    @Subscribe
    public void intEvent(Integer event) {
      log.append("(" + event + ")\n");
    }
  }

  // https://www.baeldung.com/guava-eventbus
  public static void test_EventBus_20240313() {
    EventBus eventBus = new EventBus();
    EventListener listener = new EventListener();
    eventBus.register(listener);
    eventBus.post("hello");
    assertEquals("hello\n", listener.log.toString());
    eventBus.post(Integer.valueOf(5));
    assertEquals("hello\n(5)\n", listener.log.toString());
  }

  public static void test_Iterators_20240312() throws NoSuchElementException {
    List<String> list = new ArrayList<>();
    try {
      Iterators.getOnlyElement(list.iterator());
      fail();
    } catch (NoSuchElementException e) {
      // Expected failure
    }
    list.add("A");
    assertEquals("A", Iterators.getOnlyElement(list.iterator()));
    list.add("B");
    try {
      Iterators.getOnlyElement(list.iterator());
      fail();
    } catch (java.lang.IllegalArgumentException e) {
      // Expected failure
    }
  }

  public static void test_LoadingCache_20240307() throws ExecutionException {
    final StringBuilder log = new StringBuilder();
    LoadingCache<String, String> cache = CacheBuilder.newBuilder()
        .maximumSize(2)
        .build(new CacheLoader<String, String>() {
          @Override
          public String load(String key) {
            log.append(key + ",");
            return "value-" + key.substring(0, 1).toLowerCase();
          }
        });
    // TODO: RemovalListener
    assertEquals("value-a", cache.get("A"));
    assertEquals("A,", log.toString());
    assertEquals("value-b", cache.get("B"));
    assertEquals("A,B,", log.toString());
    assertEquals("value-a", cache.get("A"));
    assertEquals("A,B,", log.toString());
    assertEquals("value-c", cache.get("C"));
    assertEquals("A,B,C,", log.toString());
    assertEquals("value-a", cache.get("A"));
    assertEquals("A,B,C,", log.toString());
  }

  // https://guava.dev/releases/23.0/api/docs/com/google/common/collect/Multimap.html
  public static void test_Multimap_20240307() {
    Multimap<String, String> multimap = ArrayListMultimap.create();
    multimap.put("A", "a1");
    multimap.put("A", "a1");
    multimap.put("B", "b1");
    assertEquals(3, multimap.size());
    Collection<String> value = multimap.get("A");
    assertEquals(2, value.size());
    assertEquals("[a1, a1]", value.toString());
  }

  // mini-test begin

  public static boolean verbose;
  public static boolean failed;

  public static final void fail() { fail("Test FAILED!"); }

  public static final void fail(String message) {
    failed = true;
    System.err.println(message);
    // throw new AssertionError(message);
  }

  public static final void assertTrue(boolean b) { if (!b) fail("assertTrue failed"); }
  public static final void assertFalse(boolean b) { if (b) fail("assertFalse failed"); }

  private static void assertDoubleEquals(double expected, double actual, double delta) {
    if (Double.compare(expected, actual) == 0 ||
        Math.abs(expected - actual) <= delta) {
      if (verbose) System.out.println(actual);
      return;
    }
    fail("expected:" + expected + "\n  actual:" + actual);
  }

  public static final void assertEquals(double expected, double actual, double delta) {
    assertDoubleEquals(expected, actual, delta);
  }

  private static void assertLongEquals(long expected, long actual) {
    if (expected == actual) {
      if (verbose) System.out.println(actual);
      return;
    }
    fail("expected:" + expected + "\n  actual:" + actual);
  }

  public static final void assertEquals(int expected, int actual) {assertLongEquals(expected, actual);}
  public static final void assertEquals(int expected, Integer actual) {assertLongEquals(expected, actual);}
  public static final void assertEquals(int expected, long actual) {assertLongEquals(expected, actual);}
  public static final void assertEquals(long expected, Long actual) {assertLongEquals(expected, actual);}

  private static final void assertObjectEquals(Object expected, Object actual) {
    if (expected == actual
        || (expected != null && expected.equals(actual))) {
      if (verbose) System.out.println(actual);
      return;
    }
    fail("expected:" + expected + "\n  actual:" + actual);
  }

  public static final void assertEquals(Object expected, Object actual) {
    assertObjectEquals(expected, actual);
  }

  public static void runTests(Class<?> cls) throws
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    Method[] methods = cls.getDeclaredMethods();
    for (int i = 0; i < methods.length; i++) {
      String name = methods[i].getName();
      if (name.startsWith("test")) {
        if (verbose) System.out.println("=== " + name);
        methods[i].invoke(null);
      }
    }
  }

  public static void main(String[] args, Class cls) throws Exception {
    if (args.length > 0 && "-v".equals(args[0])) verbose = true;
    runTests(cls);
    if (failed) System.exit(1);
  }

  // mini-test end

  public static void main(String[] args) throws Exception {
    main(args, GuavaSnippet.class);
  }
}
