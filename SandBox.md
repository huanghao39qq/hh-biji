
# 2024/04/28

## 测试MD书写格式

- 真
- 善
- 美

**重点**是，要能够简单地发布内容。

是否可以嵌入代码，例如 `lsblk /dev/sda` ，测试一下。

测试链接 [百度](https://www.baidu.com "baidu")。

测试图片

`![输入图片说明](url "图片标题")`

![博客园Logo](https://i-beta.cnblogs.com/assets/adminlogo.gif "图片标题")

参考：
Gitee中 README.md文件的编辑和使用 - 糯米雪梨
https://www.cnblogs.com/IT-Ramon/p/12015607.html


代码片段

```
println('Hello, world!')
```

> 引用文字
> 更多内容

## 参考资料

Markdown 解析器变更 | Gitee 产品文档
https://help.gitee.com/enterprise/wiki/markdown-parser-changes

附 CommonMark 语法文档 http://commonmark.org/help/
