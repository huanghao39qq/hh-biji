
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// Java 8+ compatible. Do not include Java 11 / 17 / 21 features here.
public class JavaSnippet {

  public static void test_202404() {
  }

  // https://docs.oracle.com/javase/8/docs/api/java/lang/String.html#split-java.lang.String-int-
  public static void test_20250205_String_split() {
    assertEquals("b,,:and:f", String.join(",", "boo:and:foo".split("o")));
    assertEquals("b,,:and:f,,", String.join(",", "boo:and:foo".split("o", -1)));
    assertEquals("boo:and:foo", String.join(",", "boo:and:foo".split("o", 1)));
    assertEquals("b,o:and:foo", String.join(",", "boo:and:foo".split("o", 2)));

    assertEquals("boo,and,foo", String.join(",", "boo:and:foo".split(":", -1)));
    assertEquals("boo,and,foo,", String.join(",", "boo:and:foo:".split(":", -1)));

    assertEquals(3, "boo:and:foo:::".split(":").length);
    assertEquals(3, "boo:and:foo".split(":", -1).length);
    assertEquals(4, "boo:and:foo:".split(":", -1).length);
    assertEquals("", "boo:and:foo:".split(":", -1)[3]);

    assertEquals(1, "".split(":").length);
    assertEquals(1, "".split(":", -1).length);
    assertEquals("", "".split(":")[0]);
  }

  // https://docs.oracle.com/javase/8/docs/api/java/util/stream/Stream.html
  public static void test_Stream_20240918() {
    List<Integer> numbers1 = Arrays.asList(1, 2, 3, 4);
    List<Integer> numbers2 = Arrays.asList(5, 6);
    Stream<Integer> stream = Stream.concat(numbers2.stream(), numbers1.stream());
    // assertEquals(6, stream.count());
    assertEquals("[5, 6, 1, 2, 3, 4]",
        stream.collect(Collectors.toList()).toString());

    assertTrue(numbers2.stream().anyMatch(e -> e == 5));
    assertTrue(numbers2.stream().allMatch(e -> e >= 5));
    assertFalse(numbers1.stream().anyMatch(e -> e == 5));
    assertFalse(numbers1.stream().allMatch(e -> e >= 5));

    numbers2 = Arrays.asList();
    assertEquals(0, numbers2.stream().collect(Collectors.toList()).size());
    assertEquals(0, numbers2.stream().map(Object::toString).collect(Collectors.toList()).size());
  }

  public static void test_20240705() {
    long m = Long.MAX_VALUE;
    int n = (int)m;
    assertEquals(-1, n);
    try {
      n = Integer.parseInt(String.valueOf(m));
      fail();
    } catch (NumberFormatException e) {
      // expected
    }
    List<String> list = new ArrayList<>();
    list.add("a");
    list.add("b");
    assertEquals("[a, b]", list.toString());
  }

  // https://stackoverflow.com/questions/1883345/whats-up-with-javas-n-in-printf
  // https://docs.oracle.com/javase/tutorial/java/data/numberformat.html
  // https://docs.oracle.com/javase/1.5.0/docs/api/java/util/Formatter.html
  public static void test_String_format_20240702() {
    // 'n'  the platform-specific line separator as returned by
    //      System.getProperty("line.separator").
    String s = String.format("1%n2");
    if (System.getProperty("line.separator").equals("\r\n")) {
      assertEquals("1\r\n2", s);
      assertEquals("Windows", "Windows");
    } else {
      assertEquals("1\n2", s);
      assertEquals("*nix", "*nix");
    }
  }

  // https://docs.oracle.com/javase%2Ftutorial%2F/java/IandI/defaultmethods.html
  static interface MyEnum {
    default String lowerCaseName() {
      return toString().toLowerCase();
    }

    enum Color implements MyEnum {
      RED,
      GREEN,
      BLUE;
    }
  }

  public static void test_enum_20240701() {
    MyEnum.Color c = MyEnum.Color.RED;
    assertEquals("RED", c.name());
    assertEquals("red", c.lowerCaseName());
  }

  // https://docs.oracle.com/javase/8/docs/api/java/util/Formatter.html
  public static void test_String_format_20240425() {
    // test null for %s
    assertEquals("(null)", String.format("(%s)", (String)null));
    assertEquals("(null)", String.format("(%d)", (Integer)null));
    assertEquals("(null)", String.format("(%f)", (Double)null));

    assertEquals("(0.100000)", String.format("(%f)", 0.1));

    // Type mis-match cases:
    assertEquals("(null)", String.format("(%d)", (String)null));

    // Special float values:
    assertEquals("(NaN)", String.format("(%f)", Double.NaN));
    assertEquals("(Infinity)", String.format("(%f)", Double.POSITIVE_INFINITY));
    assertEquals("(-Infinity)", String.format("(%f)", Double.NEGATIVE_INFINITY));
    assertEquals("(+0.000000)", String.format("(%+f)", Double.MIN_VALUE));
    assertEquals("(+0.000000)", String.format("(%+f)", 0.0));
    assertEquals("(-0.000000)", String.format("(%+f)", -0.0));
    assertTrue(0.0 == -0.0);
    double d = -0.0;
    assertEquals("(-0.000000)", String.format("(%+f)", d));
    d -= 0.0;
    assertEquals("(-0.000000)", String.format("(%+f)", d));
    d += 0.0;
    NOTICE_EQUALS("(+0.000000)", String.format("(%+f)", d));

    assertEquals("(NaN)", String.format("(%.2f)", Float.NaN));
    assertEquals("(Infinity)", String.format("(%.2f)", Float.POSITIVE_INFINITY));
    assertEquals("(-0.00)", String.format("(%+.2f)", -Float.MIN_VALUE));
  }

  // https://docs.oracle.com/javase/9/docs/api/java/util/List.html#of--
  // https://kotlinlang.org/docs/java-to-kotlin-idioms-strings.html
  public static void test_stream_and_Collectors_20240403() {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
    // List.of(1, 2, 3, 4, 5, 6);  // List.of (Since: 9)
    String invertedOddNumbers = numbers
        .stream()
        .filter(it -> it % 2 != 0)
        .map(it -> -it)
        .map(Object::toString)
        .collect(Collectors.joining("; "));
    assertEquals("-1; -3; -5", invertedOddNumbers);
  }

  public static class MyFile implements java.lang.AutoCloseable {
    public static StringBuffer log = new StringBuffer();
    public String name = "";
    @Override
    public void close() throws Exception {
      log.append(name + " closed.\n");
    }
  }

  // https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html
  public static void test_try_with_20240314() throws Exception {
    MyFile.log.setLength(0);
    try (MyFile f = new MyFile()) {
      f.name = "A";
    }
    assertEquals("A closed.\n", MyFile.log.toString());
  }

  public static void test_string_in_switch_20240313() {
    // Supported in Java 7+
    // NEW: Java 12 switch ->, yield
    int n = 0;
    String str = "two";
    switch (str) {
      case "one": n = 1; break;
      case "two": n = 2; break;
      default:    n = 3; break;
      }
    assertEquals(2, n);
  }

  // https://docs.oracle.com/javase/8/docs/api/java/util/stream/package-summary.html
  public static void test_stream_filter_20240312() {
    ArrayList<Integer> list = new ArrayList<>();
    Collections.addAll(list, 1, 2, 3, 4, 5);
    assertEquals("[2, 4]", list.stream()
        .filter(n -> (n % 2) == 0)
        .collect(Collectors.toList()).toString());
    assertEquals(30, list.stream()
        .mapToInt(n -> n * 2)
        .sum());
  }

  public static void test_catching_multiple_exception_types_20240305() {
    String s = "1";
    try {
      if ("1".equals(s)) throw new RuntimeException("A");
      throw new IOException("B");
    } catch (RuntimeException | IOException e) {
      assertEquals("A", e.getMessage());
    }

    Exception ioException = new IOException();
    assertFalse(ioException instanceof RuntimeException);
  }

  // https://docs.oracle.com/javase/8/docs/api/java/util/function/package-summary.html
  // https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/function/package-summary.html
  public static void test_FunctionalInterface_20240304() {
    Predicate<String> p = String::isEmpty;
    assertTrue(p.test(""));

    // 2024-3-25 TODO:
    List<String> list = Arrays.asList("a", "bb", "ccc");
    // assertEquals("[1, 2, 3]",
    //     list.stream().map((ToIntFunction) e -> e.length())
    //         .collect(Collectors.toList()).toString());
  }

  public static void test_FutureTask_20240304() throws
      InterruptedException, ExecutionException {
    // This lambda syntax will not create an inner class.
    Callable<String> callable2 = () -> "lambda";

    // This will create an inner class. Check it with:
    //   javap -cp . 'JavaSnippet$1'
    //   javap -p -c -cp . 'JavaSnippet$1'
    // In JDK 11, it is valid:  new Callable<>()
    Callable<String> callable = new Callable<String>() {
      @Override
      public String call() throws Exception {
        Thread.sleep(100);
        return "OK";
      }
    };
    FutureTask task = new FutureTask<>(callable);
    new Thread(task).start();
    Thread.sleep(100);
    if (!task.isDone()) {
      Thread.sleep(50);
    }
    assertTrue(task.isDone());
    assertFalse(task.isCancelled());
    assertEquals("OK", task.get());
  }

  // https://docs.oracle.com/javase/8/docs/api/java/util/Optional.html
  public static void test_Optional_20240228() {
    Optional<Integer> value = Optional.of(1);
    assertTrue(value.isPresent()); // isEmpty() added since: 11
    assertEquals(1, value.get());
    assertEquals(1, value.orElse(2));

    // 2024-03-12
    value = Optional.empty();
    assertFalse(value.isPresent());
    assertEquals(2, value.orElse(2));

    // 2024-06-14
    Integer n = 3;
    value = Optional.ofNullable(n);
    assertEquals(3, value.get());
    n = null;
    value = Optional.ofNullable(n);
    assertFalse(value.isPresent());
    try {
      value.get();
      fail();
    } catch (NoSuchElementException e) {
      // expected
    }
    assertEquals(null, value.orElse(null));

    try {
      n = null;
      value = Optional.of(n);
      fail();
    } catch (NullPointerException e) {
      // expected
    }

    // 2024-09-18
    final StringBuilder sb = new StringBuilder();
    Optional<Integer> intValue = Optional.empty();
    intValue.ifPresent(sb::append);
    assertEquals("", sb.toString());
    intValue = Optional.of(65);
    intValue.ifPresent(sb::append);
    assertEquals("65", sb.toString());
    // https://docs.oracle.com/javase/8/docs/api/java/lang/StringBuilder.html
    // I am not sure which one is called above, StringBuilder.append(int) or
    // append(Object), but here it must be StringBuilder.appendCodePoint(int).
    intValue.ifPresent(sb::appendCodePoint);
    assertEquals("65A", sb.toString());
  }

  public static void test_lambda_20240228() {
    Runnable runnable = () -> System.out.printf("");
    Consumer<String> fun = (s) -> System.out.printf(s);
    BinaryOperator<Integer> add = (x,y) -> x+y;
    assertEquals(3, add.apply(1, 2));
    add = (x,y) -> {return x+y;};
    assertEquals(7, add.apply(2, 5));

    int[] sum = new int[1];
    ArrayList<Integer> list = new ArrayList<>();
    Collections.addAll(list, 1, 3, 5);
    list.forEach(e -> sum[0] += e);
    assertEquals(9, sum[0]);
    list.forEach(e -> { sum[0] += e; });
    assertEquals(18, sum[0]);
    list.sort((x,y) -> y - x);
    assertEquals("[5, 3, 1]", list.toString());
  }

  @Deprecated
  public void method1() {
  }

  public static void test_annotation_20240228() throws
      NoSuchMethodException, SecurityException {
    Class<?> cls = JavaSnippet.class;
    Method method1 = cls.getDeclaredMethod("method1");
    assertTrue(method1 != null);
    assertEquals(Void.TYPE, method1.getReturnType());

    Annotation[] a = method1.getAnnotations();
    assertEquals(1, a.length);
    assertEquals(Deprecated.class, a[0].annotationType());
    a = method1.getAnnotationsByType(Deprecated.class);
    assertEquals(1, a.length);
  }

  public static void test_20240228() {
    assertEquals(1, Integer.valueOf(1));
    assertEquals(2, 2L);
    // assertEquals(1.0, 1); // This will FAIL, as it invokes assertObjectEquals.
    // assertEquals(1, 1.0); // same as above

    // assertEquals(0.0, -0.0); // FAIL!
    assertTrue(0.0 == -0.0);
    assertEquals(0.0, -0.0, 0);
    assertEquals(1.0, 1, 0);
  }

  // mini-test begin

  public static boolean verbose;
  public static boolean failed;

  public static final void fail() { fail("Test FAILED!"); }

  public static final void fail(String message) {
    failed = true;
    System.err.println(message);
    // throw new AssertionError(message);
  }

  public static final void assertTrue(boolean b) { if (!b) fail("assertTrue failed"); }
  public static final void assertFalse(boolean b) { if (b) fail("assertFalse failed"); }

  private static void assertDoubleEquals(double expected, double actual, double delta) {
    if (Double.compare(expected, actual) == 0 ||
        Math.abs(expected - actual) <= delta) {
      if (verbose) System.out.println(actual);
      return;
    }
    fail("expected:" + expected + "\n  actual:" + actual);
  }

  private static void assertLongEquals(long expected, long actual) {
    if (expected == actual) {
      if (verbose) System.out.println(actual);
      return;
    }
    fail("expected:" + expected + "\n  actual:" + actual);
  }

  private static final void assertObjectEquals(Object expected, Object actual) {
    if (expected == actual
        || (expected != null && expected.equals(actual))) {
      if (verbose) System.out.println(actual);
      return;
    }
    fail("expected:" + expected + "\n  actual:" + actual);
  }

  public static final void assertEquals(double expected, double actual, double delta) {
    assertDoubleEquals(expected, actual, delta);
  }

  public static final void assertEquals(int expected, Integer actual) {assertLongEquals(expected, actual);}
  public static final void assertEquals(int expected, Long actual) {assertLongEquals(expected, actual);}
  public static final void assertEquals(long expected, long actual) {assertLongEquals(expected, actual);}

  public static final <T> void assertEquals(T expected, T actual) {assertObjectEquals(expected, actual);}
  public static final <T> void NOTICE_EQUALS(T expected, T actual) {assertObjectEquals(expected, actual);}

  public static void runTests(Class<?> cls) throws
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    Method[] methods = cls.getDeclaredMethods();
    for (int i = 0; i < methods.length; i++) {
      String name = methods[i].getName();
      if (name.startsWith("test")) {
        if (verbose) System.out.println("=== " + name);
        methods[i].invoke(null);
      }
    }
  }

  public static void main(String[] args, Class cls) throws Exception {
    if (args.length > 0 && "-v".equals(args[0])) verbose = true;
    runTests(cls);
    if (failed) System.exit(1);
  }

  // mini-test end

  public static void main(String[] args) throws Exception {
    main(args, JavaSnippet.class);
  }
}
