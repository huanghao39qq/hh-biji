
/**
 * Build and run:
 * p=~/Downloads
 * javac -cp .:$p/mockito-core-5.10.0.jar MockitoSnippet.java
 * java -cp .:$p/mockito-core-5.10.0.jar:$p/byte-buddy-1.14.11.jar:$p/byte-buddy-agent-1.14.11.jar:$p/objenesis-3.3.jar MockitoSnippet
 */

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.*;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.exceptions.verification.NoInteractionsWanted;
//port org.mockito.exceptions.verification.TooManyActualInvocations;
import org.mockito.exceptions.verification.VerificationInOrderFailure;

public class MockitoSnippet extends JavaSnippet {

  static class Dummy2 {
    public final String name() {
      return "A";
    }
  }

  static final class Dummy3 {
    public final String name() {
      return "BB";
    }
  }

  // final class/method CAN be mocked!
  public static void test_final_20240709() {
    Dummy2 mockDummy2 = mock(Dummy2.class);
    when(mockDummy2.name()).thenReturn("B");
    assertEquals("B", mockDummy2.name());

    Dummy3 mockDummy3 = mock(Dummy3.class);
    when(mockDummy3.name()).thenReturn("C");
    assertEquals("C", mockDummy3.name());

    assertEquals("B", mockDummy2.name());
    assertEquals("C", mockDummy3.name());
  }

  // https://site.mockito.org/javadoc/current/org/mockito/InOrder.html
  // https://stackoverflow.com/questions/21901368/mockito-verify-order-sequence-of-method-calls
  public static void test_inOrder_20240702() {
    List list = mock(List.class);
    list.add("A");
    list.add("B");
    list.add("C");
    list.add("D");

    InOrder inOrder = Mockito.inOrder(list);
    inOrder.verify(list).add(eq("A"));
    inOrder.verify(list).add(eq("C"));

    try {
      inOrder.verify(list).add(eq("B"));
      fail();
    } catch (VerificationInOrderFailure e) {
      // expected
    }

    inOrder.verify(list).add(anyString());
    inOrder.verifyNoMoreInteractions();

    try {
      Mockito.verifyNoMoreInteractions(list);
      fail();
    } catch (NoInteractionsWanted e) {
      // expected
    }

    verify(list).add(eq("B"));
    Mockito.verifyNoMoreInteractions(list);
  }

  // https://java2blog.com/mockito-spy/
  public static void test_spy_20240322() {
  	Map map = new HashMap();
  	map = Mockito.spy(map);
  	map.put("a", 1);
  	verify(map).put("a", 1);
  	assertEquals(1, map.size());

  	Mockito.doReturn(5).when(map).size();
  	assertEquals(5, map.size());
  }

  // https://www.baeldung.com/mockito-exceptions
  public static void test_doThrow_when_20240322() {
    List list = mock(List.class);
    list.clear();
    doThrow(IllegalStateException.class).when(list).clear();
    try {
      list.clear();
      fail();
    } catch (IllegalStateException e) {
      // Expected
    }

    list.get(0);
    doThrow(new IllegalStateException("Error 001")).when(list).get(anyInt());
    try {
      list.get(0);
      fail();
    } catch (IllegalStateException e) {
      // Expected
      assertEquals("Error 001", e.getMessage());
    }
  }

  // https://www.baeldung.com/mockito-behavior
  public static void test_doReturn_doAnswer_when_20240322() {
    List list = mock(List.class);
    doReturn(false).when(list).add(anyString());
    assertEquals(false, list.add("a"));

    doAnswer(invocation -> "same").when(list).get(anyInt());
    assertEquals("same", list.get(5));
  }

  public static void test_when_thenReturn_20240708() {
    List list = mock(List.class);
    when(list.get(1)).thenReturn("B2");
    assertEquals("B2", list.get(1));
    assertEquals("B2", list.get(1));

    // default return value: false, null, 0
    assertEquals(true, list.equals(list));
    assertEquals(false, list.contains("B2"));
    assertEquals(null, list.get(2));
    assertEquals(0, list.size());

    // The last `thenReturn` will overwrite the previous calls.
    when(list.get(1)).thenReturn("B3");
    when(list.get(1)).thenReturn("B4");
    assertEquals("B4", list.get(1));
    assertEquals("B4", list.get(1));

    // This chain style is different from above.
    when(list.get(1)).thenReturn("B5").thenReturn("B6");
    assertEquals("B5", list.get(1));
    assertEquals("B6", list.get(1));
    assertEquals("B6", list.get(1));
  }

  // https://java2blog.com/mockito-example/
  public static void test_when_thenReturn_20240322() {
  	Iterator it = mock(Iterator.class);
  	when(it.next()).thenReturn("Hello").thenReturn("World");
    assertEquals("Hello", it.next());
    assertEquals("World", it.next());
    assertEquals("World", it.next());

    List list = mock(List.class);
    when(list.get(1)).thenReturn("Second element");
    assertEquals(null, list.get(0));
    assertEquals("Second element", list.get(1));
    assertEquals("Second element", list.get(1));
    assertEquals(0, list.size());
    verify(list).get(0);
    verify(list, times(2)).get(1);

    when(list.get(anyInt())).thenReturn("dummy");
    assertEquals("dummy", list.get(0));
    assertEquals("dummy", list.get(1));
  }

  // https://www.baeldung.com/mockito-annotations#mock-annotation
  public static void test_whenNotUseMockAnnotation_thenCorrect() {
    List mockList = Mockito.mock(ArrayList.class);

    mockList.add("one");
    Mockito.verify(mockList).add("one");
    assertEquals(0, mockList.size());

    Mockito.when(mockList.size()).thenReturn(100);
    assertEquals(100, mockList.size());
    assertEquals(100, mockList.size());
  }

  static class Dummy1 {
    @Mock
    List<String> mockedList;
  }

  public static void test_whenUseMockAnnotation_thenMockIsInjected() {
  	Dummy1 obj = new Dummy1();
  	assertEquals(null, obj.mockedList);
    MockitoAnnotations.openMocks(obj);
    obj.mockedList.add("one");
    Mockito.verify(obj.mockedList).add("one");
    assertEquals(0, obj.mockedList.size());
  }

  public static void main(String[] args) throws Exception {
    main(args, MockitoSnippet.class);
  }
}
