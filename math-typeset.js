
/**
 * Check location search and hash for "size-A4", "dev-kindle",
 * "print", "screen", "safari", "chrome", etc.,
 * and change body's class accordingly.
 */
function changeBodyClassBySearch() {
  var search = location.search + location.hash;  // eg: "?search#hash"
  if (search.match(/\bsize-A4\b/)) {
    __removeSizeClass();
    document.body.classList.add('size-A4');
  }
  if (search.match(/\bsize-A5\b/)) {
    __removeSizeClass();
    document.body.classList.add('size-A5');
  }
  if (search.match(/\bprint\b/)) {
    document.body.classList.add('print');
    document.body.classList.remove('screen');
  }
  if (search.match(/\bscreen\b/)) {
    document.body.classList.add('screen');
    document.body.classList.remove('print');
  }
  if (search.match(/\bsafari\b/)) {
    document.body.classList.add('safari');
    document.body.classList.remove('chrome');
  }
  if (search.match(/\bchrome\b/)) {
    document.body.classList.add('chrome');
    document.body.classList.remove('safari');
  }
  if (search.match(/\bdev-kindle\b/)) {
    __removeDevClass();
    document.body.classList.add('dev-kindle');
  }

  function __removeSizeClass() {
    document.body.className = document.body.className.replace(/\bsize-[^ ]*/g, '');
  }
  function __removeDevClass() {
    document.body.className = document.body.className.replace(/\bdev-[^ ]*/g, '');
  }
}

/**
 * Replace matched text with tag nodes.
 *
 * @param pattern a RegExp object, eg. /a/g
 * @param templateNode use cloneNode to create new nodes
 * @param allowedTags whitelist of tags to iterate through, eg. ['p']
 * @param excludedClasses list of classes not to do replace (TODO)
 */
function replaceTextWithTaggedText(
    root, pattern, templateNode, allowedTags, excludedClasses) {
  if (!allowedTags) allowedTags = [];
  if (!excludedClasses) excludedClasses = [];

  var childNodes = root.childNodes;
  for (var index = 0; index < childNodes.length; index++) {
    var node = childNodes[index];
    if (node.nodeName == '#text') {
      var expandedArray = _doReplace(node.textContent);
      if (expandedArray && expandedArray.length > 1) {
        var nextNode = null;
        if (index < (childNodes.length - 1)) {
          nextNode = childNodes[index + 1];
        }
        node.textContent = expandedArray[0];
        for (var i = 1; i < expandedArray.length; i++) {
          root.insertBefore(expandedArray[i], nextNode);
        }
        index += expandedArray.length - 1;
        expandedArray = null;
      }
    } else {
      if (-1 != allowedTags.indexOf(node.nodeName)) {
        replaceTextWithTaggedText(
            node, pattern, templateNode, allowedTags, excludedClasses);
      }
    }
  }

  // @return The first is string, the rest are created elements.
  function _doReplace(text) {
    pattern.lastIndex = 0;
    if (!pattern.test(text)) return null;
    pattern.lastIndex = 0;  // reset the pattern!

    var re = pattern;
    var expandedArray = [];
    var array, lastIndex = 0;
    array = re.exec(text);
    for (; 0 != re.lastIndex; array = re.exec(text)) {
      var matched = array[0];
      if (lastIndex == 0) {
        expandedArray.push(text.substring(0, re.lastIndex - matched.length));
      } else if ((re.lastIndex - matched.length) > lastIndex) {
        var e = document.createTextNode(
            text.substring(lastIndex, re.lastIndex - matched.length));
        expandedArray.push(e);
      }

      var e = templateNode.cloneNode();
      e.textContent = matched;
      expandedArray.push(e);

      lastIndex = re.lastIndex;
    }

    if (text.length > lastIndex) {
      var e = document.createTextNode(text.substring(lastIndex, text.length));
      expandedArray.push(e);
    }

    return expandedArray;
  }
}
