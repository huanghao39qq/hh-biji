
// mini-test begin
var VERBOSE;

if ("undefined" == typeof fail) fail =
function fail(failureMessage) {
  console.error(failureMessage);
  return false;
}

function assertTrue(valueOrFunc) {
  var value = valueOrFunc;
  if ("function" == typeof valueOrFunc) value = valueOrFunc();
  if (value) return true;
  return fail("assertTrue failed: " + valueOrFunc);
}

function assertFalse(valueOrFunc) {
  var value = valueOrFunc;
  if ("function" == typeof valueOrFunc) value = valueOrFunc();
  if (value) return fail("assertFalse failed: " + valueOrFunc);
  return true;
}

function assertEquals(expected, actual) {
  if (VERBOSE) console.log(actual);
  if (expected == actual) return true;
  return fail("assertEquals failed:\n"
      + "arg1 : " + expected + "\n" + "arg2 : " + actual);
      //+ "\n----\n" + String(assertEquals.caller).replace(/\r/g, ""));
}

var __latest_test;

function TESTCASE(theFunction) {
  if (__latest_test) {
    try {
      __latest_test();
    } catch (e) {
      fail("" + e);
    }
  }
  __latest_test = theFunction;
}
// mini-test end

TESTCASE(testAssert)
function testAssert() {
  var v;
  assertTrue(!false);
  assertFalse(v);
  assertEquals(18, 0x12);
  assertTrue(function() {return "a" < "b"});
}

TESTCASE(dumpError)
function dumpError() {
  var err = new Error('');
  console.log(err.stack.split('\n')[1]);
  //console.trace("Message"); // with stacktrace
}

TESTCASE()  // KEEP THIS TO RUN THE LAST TEST
