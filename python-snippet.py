#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

# mini-test-h begin
_DoTest_allTests = [];
def T_(test_func_name): global _DoTest_allTests; _DoTest_allTests.append(test_func_name)
# mini-test-h end

T_('test_202403')
def test_202403():
    pass

class T1:
    id = 0
    name = 'A'

class T2(T1):
    age = 5

T_('test_class_20240828')
def test_class_20240828():
    t = T1()
    assertEquals(0, t.id)
    assertEquals('A', t.name)
    t.id += 1
    t.name = 'abc'
    assertEquals(1, t.id)

    tt = T1()
    assertEquals(0, tt.id)
    assertEquals('A', tt.name)

    t2 = T2()
    assertEquals(0, t2.id)
    assertEquals(5, t2.age)

T_('test_zip_20240303')
def test_zip_20240303():
    a = (1,2,3)
    b = (4,5,6)
    assertEquals([(1,4), (2,5), (3,6)], list(zip(a, b)))
    assertEquals([(1,4,1), (2,5,2), (3,6,3)], list(zip(a, b, a)))

T_('test_string_escape_20231114')
def test_string_escape_20231114():
    # Be CAREFUL when you write a regular expression!
    assertEquals('\x01 \x09',  '\1 \t')
    assertEquals('\\1 \\t',   r'\1 \t')

    assertEquals('\x01f\x01g\n', '\1f\1g\12')

    import re
    assertEquals('_\x01__\x01_', re.sub('(a.)', '_\1_', 'abac'))
    assertEquals('_ab__ac_', re.sub('(a.)', r'_\1_', 'abac'))

T_('test_timezone_20231108')
def test_timezone_20231108():
    import datetime
    now = datetime.datetime.now()
    assertEquals(None, now.tzinfo)
    if not sys.version_info.major >= 3:
        return
    # The following test won't work in python2.
    local_now = now.astimezone()
    if not local_now.tzinfo:
        fail(str(local_now))
        return

    # Dummy tests, just to print the value in verbose mode.
    str = local_now.tzname()
    assertEquals(str, local_now.tzname())
    timedelta = local_now.utcoffset()
    assertEquals(timedelta, local_now.utcoffset())

T_('test_timedelta_20231108')
def test_timedelta_20231108():
    import datetime
    timedelta = datetime.timedelta(hours=8)
    assertEquals(8*3600, timedelta.seconds)
    assertEquals(0, timedelta.days)
    assertEquals(8*3600, timedelta.total_seconds())
    assertEquals(1, 1.0)
    assertEquals(1, 3//2)

T_('test_pack_unpack_20231106')
def test_pack_unpack_20231106():
    from struct import pack, unpack
    # The optional first format char indicates byte order, size and alignment:
    #   @: native order, size & alignment (default)
    #   =: native order, std. size & alignment
    #   <: little-endian, std. size & alignment
    #   >: big-endian, std. size & alignment
    #   !: same as >

    # The remaining chars indicate types of args and must match exactly;
    # these can be preceded by a decimal repeat count:
    #   x: pad byte (no data); c:char; b:signed byte; B:unsigned byte;
    #   ?: _Bool (requires C99; if not available, char is used instead)
    #   h:short; H:unsigned short; i:int; I:unsigned int;
    #   l:long; L:unsigned long; f:float; d:double; e:half-float.
    # Special cases (preceding decimal count indicates length):
    #   s:string (array of char); p: pascal string (with count byte).
    # Special cases (only available in native format):
    #   n:ssize_t; N:size_t;
    #   P:an integer type that is wide enough to hold a pointer.
    # Special case (not in native mode unless 'long long' in platform C):
    #   q:long long; Q:unsigned long long
    # Whitespace between formats is ignored.

    assertEquals(b'\x01', pack('B', 1))
    assertEquals(b'1', pack('B', 0x31))
    assertEquals(b'12', pack('2B', 0x31, 0x32))
    assertEquals(b'\x14\x13\x02\x01', pack('<I', 0x01021314))
    assertEquals(b'\x01\x02\x13\x14', pack('>I', 0x01021314))

    assertEquals((0x31,), unpack('B', b'1'))
    assertEquals((0x31, 0x32), unpack('2B', b'12'))
    assertEquals((0x01021314,), unpack('<I', b'\x14\x13\x02\x01'))
    assertEquals(0x01021314, 16913172)

T_('test_for_loop_20231030')
def test_for_loop_20231030():
    result = ''
    for a in ((1, 'a'),
              (2, 'b'), ):
        result += ('%02d-%s ' % a)
    assertEquals('01-a 02-b ', result)

T_('test_assignment_exp_20231027')
def test_assignment_exp_20231027():
    n = 1
    # Python 3.8 will bring in PEP572 - Assignment Expressions
    # https://peps.python.org/pep-0572/
    if (sys.version_info.major == 3 and sys.version_info.minor >= 8
            ) or sys.version_info.major > 3:
        pass

    # Better way to check value pair:
    if (sys.version_info.major, sys.version_info.minor) >= (3, 8):
        a = (10, 11, 12)
        # SyntaxError: assignment expression cannot rebind comprehension iteration variable 'x'
        # assertEquals([], [x := x + 1 for x in a])

        # Python 2.7.5 won't accept these:
        # assertEquals(2, n := n + 1) # SyntaxError: invalid syntax
        n = 0
        # assertEquals([10, 21, 33], [n := x + n for x in a])
        # assertEquals(33, n)

    assertTrue((3, 9) > (3, 8))
    assertTrue((3, 8) >= (3, 8))
    assertTrue((4, 0) > (3, 8))
    assertTrue((4, 0) > (3, 8, 1))
    assertTrue((3, 8) < (3, 8, 1))

    assertEquals([3, 8], list((3, 8)))
    if [3, 8] == (3, 8): fail('ERROR: list should never equals to tuple!')

T_('test_bool_20231023')
def test_bool_20231023():
    # https://unix.stackexchange.com/questions/414042/how-to-split-an-output-to-two-files-with-grep
    a = (10, 11, 12)
    assertEquals(11, a[True])
    assertEquals(10, a[False])
    str = None
    assertEquals(11, a[str is None])
    assertEquals(10, a[str is not None])

T_('test_lambda_20231020')
def test_lambda_20231020():
    numbers = [1, 2, 3, 4, 5]
    assertEquals('[1, 4, 9, 16, 25]',
        str(list(map(lambda x: x ** 2, numbers))))
    assertEquals('[2, 4]',
        str(list(filter(lambda x: x % 2 == 0, numbers))))
    from functools import reduce
    sum_of_numbers = reduce(lambda x, y: x + y, numbers)
    assertEquals(15, sum_of_numbers)

    assertEquals('[1, 4, 9, 16, 25]',
        str([x ** 2 for x in numbers]))

    assertEquals([1, 2], [1, 2])
    assertEquals([1, 2], numbers[:2])

    # reduce(lambda x, y: a += str(y), numbers) # invalid syntax
    # reduce(lambda x, y: x + y if print(x) else x + y, numbers) # OK, debug reduce
    result = reduce(lambda x, y: str(x) + str(y), numbers)
    assertEquals('12345', result)

T_('test_chr_ord')
def test_chr_ord():
    assertEquals(65, ord('A'))
    assertEquals(' ', chr(32))
    assertEquals('c', chr(99))
    assertTrue('A' < chr(99))
    if sys.version_info.major < 3:
        # Py2:
        assertTrue('A' > 99)
    else:
        try:
            b = 'A' > 99
            fail()
        except TypeError as e:
            # Py3: unorderable types: str() > int()
            pass

# mini-test begin
from inspect import currentframe
import os;
_DoTest_verbose = 1 if os.getenv('VERBOSE') else 0
def fail(message='ERROR'):
    sys.stderr.write('%s\n' % message)
def assertTrue(b):
    if b: return True
    fail('assertTrue failed:(%d)' % currentframe().f_back.f_lineno)
def assertEquals(expected, actual):
    if expected == actual:
        global _DoTest_verbose
        if not _DoTest_verbose: return True
        actual = str(actual).replace('\\', '\\\\').replace('\r', '\\r').replace('\n', '\\n')
        sys.stdout.write('(%d):%s\n' % (currentframe().f_back.f_lineno, actual))
        return True
    expected = str(expected).replace('\\', '\\\\').replace('\r', '\\r').replace('\n', '\\n')
    actual = str(actual).replace('\\', '\\\\').replace('\r', '\\r').replace('\n', '\\n')
    fail('assertEquals failed:(%d)\nexpected:%s\n  actual:%s' % (
        currentframe().f_back.f_lineno, expected, actual))
def allTestsLocalRunner(local_vars):
    global _DoTest_allTests
    for test_func_name in _DoTest_allTests:
        local_vars[test_func_name]()
    _DoTest_allTests = []
# mini-test end

if __name__ == '__main__': allTestsLocalRunner(vars())
